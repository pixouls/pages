import time
import csv
import typer

from feedgen.feed import FeedGenerator

# Typer lets us easily create a CLI interface
# for issuing commands etc - try running
# python3 main.py --help
app = typer.Typer()

FILENAME = "../rss.xml"
POST_CSV = "post.csv"

# This decorator once again makes another command for the typer CLI interface
# So it would be:
# python3 main.py add-new
@app.command()
def add_new(filename: str = FILENAME):
    typer.echo("✨ Adding new post...")

    # TODO: generate anything else needed for a feed item
    title = typer.prompt("What's the title?")
    summary = typer.prompt("What's the summary?")
    link = typer.prompt("What's the link?")

    # Add our new post to the csv for rendering later/saving
    add_post_to_csv(title, summary, link)

    generate(filename)

    typer.echo("🌿 Finished Add-New!")

def add_post_to_csv(title: str, summary: str, link: str):
    """
    Adding a single post to the csv file
    """
    typer.echo("💾 Saving post to csv for generation in the future...")

    id = time.strftime('%Y%m%d%M%S')

    with open(POST_CSV, 'a', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow([id, title, summary, link])


def add_post_to_feed_generator(fg: FeedGenerator, id: str, title: str, summary: str, link: str):
    """
    Adding a single post to the feed generator
    """
    entry = fg.add_entry()
    entry.id(id)
    entry.title(title)
    entry.summary(summary)
    entry.link(href=link)
    # No return because side-effect of fg.add_entry() is a reference to the
    # fe object which we update in place.


# This decorator makes the following function
# a command for the typer library
# and the function arguments will be used as command line parameters
# so this would be:
# python3 main.py generate
@app.command()
def generate(filename: str = FILENAME) -> FeedGenerator:
    typer.echo(f"🛠️ Generating {filename}....")

    # The goal here is to create some XML that is valid RSS
    # See https://www.w3schools.com/xml/xml_rss.asp for an example

    # This is from the feedgen library that will generate the XML for us
    # https://github.com/lkiesow/python-feedgen#create-a-feed

    fg = FeedGenerator()

    # These set things that end up in the "channel"
    # Update them to your liking
    fg.title('This is Definitely an RSS Feed')
    fg.author({'name': 'Pixel'})
    fg.subtitle('Pixel\'s Wonderful Web Blog')
    fg.link(href='https://www.pixouls.xyz/rss.xml', rel='self')
    fg.language('en')

    # Now we write th e file
    # See https://github.com/lkiesow/python-feedgen#generate-the-feed

    # If we generate the entire rss field each time though,
    # we need to store the information needed in a post outside of the
    # rss feed itself. We can do so pretty easily in a csv I think...

    with open(POST_CSV, "r", newline='') as csvfile:
      reader = csv.DictReader(csvfile)
      for i, row in enumerate(reader):

        id = row['id']
        title = row['title']
        summary = row['summary']
        link = row['link']

        add_post_to_feed_generator(fg, id, title, summary, link)

      print(i, row)

    # Update file
    fg.rss_file(filename, pretty=True)

    typer.echo("🎉 Finished Generate!")


if __name__ == "__main__":
    app()
