# Pixel RSS

2022-04-20: Pixel RSS is a project by [Metasyn](metasyn.pw) to help guide [Pixel](pixouls.xyz) in learning how to make their own RSS feed.

RSS is free. It's a way to subscribe to updates from a website without needing to "follow" people.

For more on RSS:
- [What is RSS (Really Simple Syndication)?](https://ncase.me/rss/)
- [What RSS Feels Like](https://gilest.org/rss-feels.html)
- [You Need Feeds](https://www.youneedfeeds.com/)
- [RSS-Bridge: The RSS feed for websites missing it](https://rss-bridge.github.io/rss-bridge/index.html)

## Features
* generates rss.xml feed
* python based
* uses poetry to manage python dependencies

## Setup

* requirements: python 3, `python3-feedgen` library, `typer` library, poetry (optional)
* install poetry - see [here](https://python-poetry.org/docs/)

Using poetry
```
# Manages dependencies, virtualenv
poetry install

# activates a virtualenv
poetry shell
python3 main.py --help
```

Using whatever is going on on your system, but I think poetry is better:

```
pip install python3-feedgen typer
python3 main.py --help
```


## TODOs

- [X] write out posts to csv when saving
- [X] load all existing posts from csv when generating the main rss file
- [X] add more fields to the main channel if needed
- [X] add more fields for each time
- [ ] remove comments


## Examples

Example of using `--help`

```
~/PixelRSS$ python3 main.py --help
Usage: main.py [OPTIONS] COMMAND [ARGS]...

Options:
  --install-completion  Install completion for the current shell.
  --show-completion     Show completion for the current shell, to copy it or
                        customize the installation.
  --help                Show this message and exit.

Commands:
  add-new
  generate
```

Example of using `generate`:

```
~/PixelRSS$ python3 main.py generate
🛠️ Generating rss.xml....
🎉 Finished!
~/PixelRSS$ cat rss.xml
<?xml version='1.0' encoding='UTF-8'?>
<rss xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/" version="2.0">
  <channel>
    <title>Some Title</title>
    <link>https://www.pixouls.xyz/rss.xml</link>
    <description>This is a cool feed!</description>
    <atom:link href="https://www.pixouls.xyz/rss.xml" rel="self"/>
    <docs>http://www.rssboard.org/rss-specification</docs>
    <generator>python-feedgen</generator>
    <image>
      <url>http://ex.com/logo.jpg</url>
      <title>Some Title</title>
      <link>https://www.pixouls.xyz/rss.xml</link>
    </image>
    <language>en</language>
    <lastBuildDate>Sat, 29 Jan 2022 19:31:43 +0000</lastBuildDate>
  </channel>
</rss>
```


Example of using `add-new`

```
~/PixelRSS$ python3 main.py add-new
✨ Adding new post...
🛠️ Generating rss.xml....
🎉 Finished!
What's the title?: PERMACULTURE
What's the summary?: earthen-ware homes are l33t
What's the link?: cool.link.foo/123
💾 Saving post to csv for generation in the future...
🌿 Finished!
~/PixelRSS$ cat rss.xml
<?xml version='1.0' encoding='UTF-8'?>
<rss xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/" version="2.0">
  <channel>
    <title>Some Title</title>
    <link>https://www.pixouls.xyz/rss.xml</link>
    <description>This is a cool feed!</description>
    <atom:link href="https://www.pixouls.xyz/rss.xml" rel="self"/>
    <docs>http://www.rssboard.org/rss-specification</docs>
    <generator>python-feedgen</generator>
    <image>
      <url>http://ex.com/logo.jpg</url>
      <title>Some Title</title>
      <link>https://www.pixouls.xyz/rss.xml</link>
    </image>
    <language>en</language>
    <lastBuildDate>Sat, 29 Jan 2022 19:32:04 +0000</lastBuildDate>
    <item>
      <title>PERMACULTURE</title>
      <link>cool.link.foo/123</link>
      <description>earthen-ware homes are l33t</description>
      <guid isPermaLink="false">123</guid>
    </item>
  </channel>
</rss>
```
